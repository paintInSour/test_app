import { Box, Typography } from "@mui/material";
import React, { useState } from "react";
import AppMenu from "./menu/AppMenu";
import QuestionBlock from "./question/QuestionBlock";
import file from "./aspirant.json";
import "./styles/App.scss";
import SearchPage from "./menu/SearchPage";

function getDefaultSettings() {
  const defaultSettings = { isRandomQuestion: false, questionRange: { from: 1, to: 500 } };
  return defaultSettings;
}

function getQuestionsMode(questionSetSettings) {
  return questionSetSettings.isRandomQuestion ? "RANDOM" : "ORDERED";
}

function getQuestions(questions, range) {
  return questions.slice(range.from - 1, range.to);
}

const notFoundImage = require("./images/pepe_cry.png");
export const TEST_PAGE = "TESTS";
export const SEARCH_PAGE = "SEARCH";

function App() {
  const [questionSetSettings, setQuestionSetSettings] = useState({ isRandomQuestion: false, questionRange: { from: 1, to: 500 } });
  const [pageSelected, setPageSelected] = useState(TEST_PAGE);

  function updateQuestionSetSettingsCallback(settings) {
    setQuestionSetSettings(settings);
  }

  function showPageSelected() {
    if (pageSelected === TEST_PAGE) {
      console.log("selected test page");
      return (
        <Box display="flex" justifyContent="flex-end">
          <QuestionBlock
            questions={getQuestions(file.questions, questionSetSettings.questionRange)}
            mode={getQuestionsMode(questionSetSettings)}
          ></QuestionBlock>
        </Box>
      );
    } else if (pageSelected === SEARCH_PAGE) {
      console.log("selected search");
      return (
        <SearchPage questionList={file.questions}></SearchPage>
        // <Box mt={20} display="flex" flexDirection="column" justifyContent="center" alignSelf="center">
        //   <img style={{ marginLeft: "20%" }} width="60%" height="auto" alt="notFoundImage" src={String(notFoundImage)}></img>
        //   <Typography> Not implemented yet :(</Typography>
        // </Box>
      );
    }
  }

  return (
    <div className="App">
      <AppMenu
        questionSetSettings={questionSetSettings}
        setQuestionSetSettings={updateQuestionSetSettingsCallback}
        questions={file.questions}
        setPageSelected={setPageSelected}
      ></AppMenu>
      {showPageSelected()}
    </div>
  );
}

export default App;
