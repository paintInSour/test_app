import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import { Button, Divider, Typography } from "@mui/material";
import Stack from "@mui/material/Stack";
import * as React from "react";
import { useEffect, useRef, useState } from "react";
import "../styles/question/QuestionBlock.scss";
import AnswerOptionBlock from "./AnswerOptionBlock.js";
import useRandomQuestion from "./UseRandomQuestion.js";

function randomInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function nextRandom(questions) {
  return randomInteger(0, questions.length);
}

function QuestionBlock({ questions, mode }) {
  const [questionId, setQuestionId] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState();
  const [showAnswer, setShowAnswer] = useState(false);
  const { getRandomQuestionIndex } = useRandomQuestion(questions);

  const prevQuestionsRef = useRef();

  useEffect(() => {
    if (JSON.stringify(prevQuestionsRef.current) !== JSON.stringify(questions)) {
      setQuestionId(0);
      prevQuestionsRef.current = questions;
    }
  }, [questions]);

  const currentQuestion = questions[questionId];

  function switchQuestion(newQuestionId) {
    if (newQuestionId >= 0 && questionId < questions.length - 1) {
      setQuestionId(newQuestionId);
      setShowAnswer(false);
      setSelectedAnswer(undefined);
    }
    if (mode === "RANDOM") {
      setQuestionId(newQuestionId);
      setShowAnswer(false);
      setSelectedAnswer(undefined);
    }
  }

  function submit() {
    setShowAnswer(true);
  }

  function getNav() {
    if (mode === "ORDERED") {
      return (
        <Stack className="QuestionBlockNav" justifyContent="center" direction="row" spacing={5} mt={3}>
          <Button size="large" onClick={() => switchQuestion(questionId - 1)}>
            <ArrowBackIosIcon></ArrowBackIosIcon>
          </Button>
          <Button disabled={!selectedAnswer} size="large" onClick={() => submit()} variant="contained">
            Submit
          </Button>
          <Button size="large" onClick={() => switchQuestion(questionId + 1)}>
            <ArrowForwardIosIcon></ArrowForwardIosIcon>
          </Button>
        </Stack>
      );
    } else {
      return (
        <Stack className="QuestionBlockNav" justifyContent="center" direction="row" spacing={5}>
          <Button size="large" onClick={() => submit()} variant="contained">
            Submit
          </Button>
          <Button size="large" onClick={() => switchQuestion(getRandomQuestionIndex())}>
            <ArrowForwardIosIcon></ArrowForwardIosIcon>
          </Button>
        </Stack>
      );
    }
  }
  return (
    <Stack className="QuestionBlock" direction="column" alignItems="center" justifyContent="space-between" minHeight={"90vh"} minWidth={"100vw"}>
      <Stack sx={{ overflowX: "hidden", overflowY: "scroll" }} maxHeight={"82vh"} ml={2} direction="column" justifyContent="space-between">
        <Typography className="QuestionText" gutterBottom variant="h6" maxWidth={"95vw"} mt={3}>
          {currentQuestion.question_id} {currentQuestion.question_text}
          <Divider />
        </Typography>

        <AnswerOptionBlock
          answers={currentQuestion.answers}
          setSelectedAnswer={setSelectedAnswer}
          selected={selectedAnswer}
          showCorrect={showAnswer}
          correctId={currentQuestion.correct_answer_id}
        />
      </Stack>
      {getNav()}
    </Stack>
  );
}

export default QuestionBlock;
