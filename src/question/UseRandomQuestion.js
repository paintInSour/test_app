import { useRef, useCallback, useEffect } from 'react';

const useRandomQuestion = (questions) => {
  const shownQuestionsRef = useRef([]);
  const previousQuestionsRef = useRef(questions);

  const getRandomQuestionIndex = () => {
    if (shownQuestionsRef.current.length === questions.length) {
      shownQuestionsRef.current = [];
    }

    const remainingQuestionsIndexes = questions
      .map((_, index) => index)
      .filter((index) => !shownQuestionsRef.current.includes(index));

    const randomIndex =
      remainingQuestionsIndexes[
        Math.floor(Math.random() * remainingQuestionsIndexes.length)
      ];

    shownQuestionsRef.current.push(randomIndex);
    console.log("questions size: ", shownQuestionsRef.current.length)
    return randomIndex;
  };

  const resetShownQuestions = useCallback(() => {
    shownQuestionsRef.current = [];
  }, []);

  useEffect(() => {
    if (previousQuestionsRef.current !== questions) {
      shownQuestionsRef.current = [];
      previousQuestionsRef.current = questions;
    }
  }, [questions]);

  return { getRandomQuestionIndex, resetShownQuestions };
};

export default useRandomQuestion;
