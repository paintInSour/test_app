import { Divider, FormControl, FormControlLabel, Radio, RadioGroup, Stack, Typography } from "@mui/material";
import * as React from "react";

function createAnswerOption(answer, selected, showCorrect, correctId) {
  function getSelectOption(highlightColor) {
    return (
      <React.Fragment>
        <FormControlLabel
          key={answer.answer_id}
          value={answer.answer_id}
          control={<Radio />}
          label={<Typography variant="body1">{answer.answer_text}</Typography>}
          sx={{ backgroundColor: highlightColor }}
        />
        <Divider />
      </React.Fragment>
    );
  }

  if (showCorrect) {
    //show if selected correct
    if (answer.answer_id == selected && answer.answer_id == correctId) {
      return getSelectOption("#76ff03");
      //show if selected wrong
    } else if (answer.answer_id == selected) {
      return getSelectOption("#ff784e");
    }

    // show real correct
    if (answer.answer_id == correctId && answer.answer_id != selected) {
      return getSelectOption("#52b202");
    }
  }
  //highlight selected
  if (selected == answer.answer_id) {
    return getSelectOption("#adadad");
  }

  return (
    <React.Fragment>
      <FormControlLabel
        key={answer.answer_id}
        value={answer.answer_id}
        control={<Radio />}
        checked={false}
        label={<Typography variant="body1">{answer.answer_text}</Typography>}
      />
      <Divider />
    </React.Fragment>
  );
}

export default function AnswerOptionBlock({ answers, setSelectedAnswer, selected, showCorrect, correctId }) {
  const handleChange = (event) => {
    if (event.target.value) {
      setSelectedAnswer(event.target.value);
    }
  };

  function shuffleAnswers() {
    if (!selected) {
      return answers.sort(() => Math.random() - 0.5).map((answer) => createAnswerOption(answer, selected, showCorrect, correctId));
    }
    return answers.map((answer) => createAnswerOption(answer, selected, showCorrect, correctId));
  }

  return (
    <React.Fragment>
      <Stack className={showCorrect ? "InactiveBlock" : ""} direction="column" justifyContent="center" alignItems="flex-start" mb={5} sx={{ width: "94%" }}>
        <FormControl sx={{ width: "98%" }}>
          <RadioGroup
            sx={{ with: "100%" }}
            aria-labelledby="ontrolled-radio-buttons-group"
            name="controlled-radio-buttons-group"
            onChange={handleChange}
            value={selected ?? 100}
          >
            {/* element to escape error for undefined value */}
            <FormControlLabel value={100} control={<Radio />} key={100} sx={{ display: "none" }}></FormControlLabel>
            {shuffleAnswers()}
          </RadioGroup>
        </FormControl>
      </Stack>
    </React.Fragment>
  );
}
