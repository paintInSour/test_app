import { Box, Divider, List, ListItem, ListItemText, Stack, TextField } from "@mui/material";
import React, { useRef, useState } from "react";

export default function SearchPage({ questionList }) {
  const searchString = useRef(null);
  const [foundQuestions, setFoundQuestions] = useState([]);

  const findQuestions = (event) => {
    const value = event.target.value.toLowerCase();
    searchString.current = value;
    const filtered = questionList.filter((question) => question.question_text.toLowerCase().includes(value));

    setFoundQuestions(filtered);
  };

  function getAnswerTextById(question, answerId) {
    for (let answer of question.answers) {
      if (answer.answer_id == answerId) {
        return answer.answer_text;
      }
    }
    return "Answer not found";
  }

  function highlightSearchString(text, searchString) {
    if (!searchString) return text; // If search string is empty, return the original text.

    const regex = new RegExp(`(${searchString})`, "gi"); // Create a regex with global and case-insensitive flags.
    const highlightedText = text.replace(regex, "<mark>$1</mark>"); // Replace occurrences with highlighted version.

    return highlightedText;
  }

  return (
    <Box sx={{ flexGrop: 1 }}>
      <Stack>
        <Box className="SearchInput">
          <TextField sx={{ width: "100%" }} label="Search" id="outlined-size-small" onChange={findQuestions} />
        </Box>
      </Stack>
      <List>
        {foundQuestions.map((question) => (
          <ListItem key={question.question_id}>
            <Stack>
              <ListItemText primary={<span dangerouslySetInnerHTML={{ __html: highlightSearchString(question.question_text, searchString.current) }} />} />
              <ListItemText sx={{ backgroundColor: "#b8bcc2" }}>{getAnswerTextById(question, question.correct_answer_id)}</ListItemText>
              <Divider />
            </Stack>
          </ListItem>
        ))}
      </List>
    </Box>
  );
}
