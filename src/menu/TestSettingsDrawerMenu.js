import InfoIcon from "@mui/icons-material/Info";
import MoreIcon from "@mui/icons-material/MoreVert";
import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  List,
  ListItem,
  MenuItem,
  Select,
  ClickAwayListener,
  SwipeableDrawer,
  Tooltip,
  Typography,
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import React, { useState } from "react";

export default function TestSettingsDrawerMenu({ questionSetSettings, setQuestionSetSettings }) {
  const [open, setOpen] = useState(false);

  const [openTooltip, setOpenTooltip] = React.useState(false);

  const handleTooltipClose = () => {
    setOpenTooltip(false);
  };

  const handleTooltipOpen = () => {
    setOpenTooltip(true);
  };

  const [selectedRangeValue, setSelectedRangeValue] = useState(getSelectRangeValue(questionSetSettings.questionRange));

  function handleRandomQuestionChange(event) {
    questionSetSettings.isRandomQuestion = event.target.checked;
  }

  function getSelectRangeValue(range) {
    console.log("selected ragen: ", "" + range.from + "" + range.to);
    return parseInt("" + range.from + "" + range.to);
  }

  function handleQuesionRangeSelected(event) {
    const selectedValue = event.target.value;

    const range = (() => {
      switch (selectedValue) {
        case 1100:
          return { from: 1, to: 100 };
        case 101200:
          return { from: 101, to: 200 };
        case 201300:
          return { from: 201, to: 300 };
        case 301400:
          return { from: 301, to: 400 };
        case 401500:
          return { from: 401, to: 500 };
        case 1500:
          return { from: 1, to: 500 };
        default:
          return { from: 1, to: 500 };
      }
    })();
    setSelectedRangeValue(parseInt(selectedValue));
    questionSetSettings.questionRange = range;
  }

  const toggleDrawer = (newOpen) => (event) => {
    if (event && event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
      return;
    }

    setOpen(newOpen);
  };

  function handleSaveSettings() {
    console.log("saving settings: ", questionSetSettings);
    let settings = { ...questionSetSettings };
    setQuestionSetSettings(settings);
    setOpen(false);
  }

  const questionRangeOptions = [
    { label: "1 - 100", value: 1100 },
    { label: "101 - 200", value: 101200 },
    { label: "201 - 300", value: 201300 },
    { label: "301 - 400", value: 301400 },
    { label: "401 - 500", value: 401500 },
    { label: "1 - 500", value: 1500 },
  ];

  const iOS = typeof navigator !== "undefined" && /iPad|iPhone|iPod/.test(navigator.userAgent);

  const DrawerList = (
    <Box display="flex" flexDirection="column" sx={{ width: "auto" }} height="30vh">
      <Typography mt={2} variant="h5" sx={{ textAlign: "center" }}>
        Question set settings
      </Typography>
      <List>
        <ListItem ml={5} key="showRandowQuestionsButton">
          <FormControlLabel label="Random questions" control={<Checkbox />} onChange={handleRandomQuestionChange} />
          <ClickAwayListener onClickAway={handleTooltipClose}>
            <div>
              <Tooltip
                PopperProps={{
                  disablePortal: true,
                }}
                onClose={handleTooltipClose}
                open={openTooltip}
                disableFocusListener
                disableHoverListener
                disableTouchListener
                title="Random question from the Question range"
              >
                <IconButton onClick={handleTooltipOpen}>
                  <InfoIcon/>
                </IconButton>
              </Tooltip>
            </div>
          </ClickAwayListener>
        </ListItem>
        <ListItem>
          <Select
            labelId="demo-multiple-name-label"
            id="demo-multiple-name"
            sx={{ width: "300px" }}
            value={selectedRangeValue}
            onChange={handleQuesionRangeSelected}
          >
            {questionRangeOptions.map((option) => (
              <MenuItem key={option.label} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </Select>
        </ListItem>
        <ListItem>
          <Box textAlign="center" width={"100%"}>
            <Button variant="contained" onClick={handleSaveSettings}>
              Save
            </Button>
          </Box>
        </ListItem>
      </List>
    </Box>
  );

  return (
    <div>
      <Box sx={{ display: { xs: "flex", md: "none" } }}>
        <IconButton size="large" aria-label="show more" aria-haspopup="true" onClick={toggleDrawer(true)} color="inherit">
          <MoreIcon />
        </IconButton>
        <SwipeableDrawer
          anchor="top"
          open={open}
          onClose={toggleDrawer(false)}
          onOpen={toggleDrawer(true)}
          disableBackdropTransition={!iOS}
          disableDiscovery={iOS}
        >
          {DrawerList}
        </SwipeableDrawer>
      </Box>
    </div>
  );
}
